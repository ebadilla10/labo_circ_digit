`timescale 1ns / 1ps
`include "Defintions.v"

//------------------------------------------------
module UPCOUNTER_POSEDGE # (parameter SIZE=16)
(
input wire Clock, Reset,
input wire [SIZE-1:0] Initial,
input wire Enable,
output reg [SIZE-1:0] Q
);


  always @(posedge Clock )
  begin
      if (Reset)
        Q = Initial;
      else
		begin
		if (Enable)
			Q = Q + 1;
			
		end			
  end

endmodule
//----------------------------------------------------

module FFD_POSEDGE_SYNCRONOUS_RESET # ( parameter SIZE=8 )
(
	input wire				Clock,
	input wire				Reset,
	input wire				Enable,
	input wire [SIZE-1:0]	D,
	output reg [SIZE-1:0]	Q
);
	

always @ (posedge Clock) 
begin
	if ( Reset )
		Q <= 0;
	else
	begin	
		if (Enable) 
			Q <= D; 
	end	
 
end//always

endmodule


//----------------------------------------------------------------------
module ADDER_FULL 
(
	input wire 				iA, iB, iC,
	output wire				oC, oR			
);

assign {oC, oR} = iA + iB + iC;


endmodule


//----------------------------------------------------------------------

module CELL_MULTIPLIER
(
	input wire [3:0]		iA, iB,
	output wire [7:0]		oR
);

wire wC [10:0];
wire wRT [5:0];

assign oR[0] = iA[0] & iB[0];

ADDER_FULL adder_1_1 
(
.iA( iA[1] & iB[0] ),
.iB( iA[0] & iB[1] ),
.iC( 1'b0 ),
.oR( oR[1] ),
.oC( wC[0] ) 
);

ADDER_FULL adder_1_2 
(
.iA( iA[1] & iB[1] ),
.iB( iA[2] & iB[0] ),
.iC( wC[0] ),
.oR( wRT[0] ),
.oC( wC[1] ) 
);

ADDER_FULL adder_1_3 
(
.iA( iA[2] & iB[1] ),
.iB( iA[3] & iB[0] ),
.iC( wC[1] ),
.oR( wRT[1] ),
.oC( wC[2] ) 
);

ADDER_FULL adder_1_4 
(
.iA( iA[3] & iB[1] ),
.iB( 1'b0 ),
.iC( wC[2] ),
.oR( wRT[2] ),
.oC( wC[3] ) 
);

ADDER_FULL adder_2_1 
(
.iA( iA[0] & iB[2] ),
.iB( wRT[0] ),
.iC( 1'b0 ),
.oR( oR[2] ),
.oC( wC[4] ) 
);

ADDER_FULL adder_2_2 
(
.iA( iA[1] & iB[2] ),
.iB( wRT[1] ),
.iC( wC[4] ),
.oR( wRT[3] ),
.oC( wC[5] ) 
);

ADDER_FULL adder_2_3
(
.iA( iA[2] & iB[2] ),
.iB( wRT[2] ),
.iC( wC[5] ),
.oR( wRT[4] ),
.oC( wC[6] ) 
);

ADDER_FULL adder_2_4
(
.iA( wC[3] ),
.iB( iA[3] & iB[2] ),
.iC( wC[6] ),
.oR( wRT[5] ),
.oC( wC[7] ) 
);

ADDER_FULL adder_3_1
(
.iA( iA[0] & iB[3]),
.iB( wRT[3] ),
.iC( 1'b0  ),
.oR( oR[3] ),
.oC( wC[8] ) 
);

ADDER_FULL adder_3_2
(
.iA( iA[1] & iB[3]),
.iB( wRT[4] ),
.iC( wC[8]  ),
.oR( oR[4] ),
.oC( wC[9] ) 
);

ADDER_FULL adder_3_3
(
.iA( iA[2] & iB[3]),
.iB( wRT[5] ),
.iC( wC[9]  ),
.oR( oR[5] ),
.oC( wC[10] ) 
);

ADDER_FULL adder_3_4
(
.iA( iA[3] & iB[3]),
.iB( wC[7] ),
.iC( wC[10]  ),
.oR( oR[6] ),
.oC( oR[7] ) 
);

endmodule

//----------------------------------------------------

module MUL_GENERATE
( 
	input wire [3:0]		iA, iB,
	output wire [7:0]		oR
);

genvar i, j;

wire [4:0] wC [2:0];
wire [4:0] wRT [2:0];

assign wC [0][0] = 0;
assign wC [1][0] = 0;
assign wC [2][0] = 0;

assign wC [0][4]=wRT[0][4];
assign wC [1][4]=wRT[1][4];
assign wC [2][4]=wRT[2][4];

assign oR[0] = iA[0]&iB[0];
assign oR[1] = wRT[0][0];
assign oR[2] = wRT[1][0];
assign oR[3] = wRT[2][0];
assign oR[4] = wRT[2][1];
assign oR[5] = wRT[2][2];
assign oR[6] = wRT[2][3];
assign oR[7] = wRT[2][4];

generate

	for (i = 0; i < `MAX_ROWS; i = i + 1) 
	begin : MUL_ROW
		for (j = 0; j < `MAX_COLS; j = j + 1)
			begin : MUL_COL
				if (i == 0)
					begin
					if (j < 3)
						begin
						ADDER_FULL adder
						(
						.iA( iA[j] & iB[1]),
						.iB( iA[j+1] & iB[0] ), 
						.iC( wC[i][j] ),
						.oR( wRT[i][j] ),
						.oC( wC[i][j+1] ) 
						);
						end
						
					else
						begin
						ADDER_FULL adder
						(
						.iA( iA[j] & iB[1]),
						.iB( 1'b0 ), 
						.iC( wC[i][j] ),
						.oR( wRT[i][j] ),
						.oC( wC[i][j+1] ) 
						);
						end
					end
					
				else
					begin
					ADDER_FULL adder
					(
					.iA( iA[j] & iB[i+1]),
					.iB( wRT[i-1][j+1] ), // ok
					.iC( wC[i][j] ),
					.oR( wRT[i][j] ),
					.oC( wC[i][j+1] ) 
					);
					end
			end
    end
endgenerate

endmodule

//----------------------------------------------------

module MUL_GENERATE16
( 
	input wire [15:0]		iA, iB,
	output wire [31:0]		oR
);

genvar i, j;

wire [16:0] wC [14:0];
wire [16:0] wRT [14:0];

assign oR[0] = iA[0]&iB[0];

/*
assign wC [0][0] = 0;
assign wC [1][0] = 0;
assign wC [2][0] = 0;

assign wC [0][4]=wRT[0][4];
assign wC [1][4]=wRT[1][4];
assign wC [2][4]=wRT[2][4];

assign oR[1] = wRT[0][0];
assign oR[2] = wRT[1][0];
assign oR[3] = wRT[2][0];
assign oR[4] = wRT[2][1];
assign oR[5] = wRT[2][2];
assign oR[6] = wRT[2][3];
assign oR[7] = wRT[2][4];
*/
generate

	for (i = 0; i < `MAX_ROWS_16; i = i + 1) 
	begin : MUL_ROW_16
	assign wC [i][0] = 0;///////////////////
	assign wC [i][16]=wRT[i][16];
	assign oR[i+1] = wRT[i][0];
	assign oR[i+16] = wRT[14][i+1];
		for (j = 0; j < `MAX_COLS_16; j = j + 1)
			begin : MUL_COL_16
				if (i == 0)
					begin
					if (j < 15)
						begin
						ADDER_FULL adder
						(
						.iA( iA[j] & iB[1]),
						.iB( iA[j+1] & iB[0] ), 
						.iC( wC[i][j] ),
						.oR( wRT[i][j] ),
						.oC( wC[i][j+1] ) 
						);
						end
						
					else
						begin
						ADDER_FULL adder
						(
						.iA( iA[j] & iB[1]),
						.iB( 1'b0 ), 
						.iC( wC[i][j] ),
						.oR( wRT[i][j] ),
						.oC( wC[i][j+1] ) 
						);
						end
					end
					
				else
					begin
					ADDER_FULL adder
					(
					.iA( iA[j] & iB[i+1]),
					.iB( wRT[i-1][j+1] ), // ok
					.iC( wC[i][j] ),
					.oR( wRT[i][j] ),
					.oC( wC[i][j+1] ) 
					);
					end
			end
    end
endgenerate

endmodule

//----------------------------------------------------
module MUX_FULL_PARALLEL # (parameter SIZE = 4)
(
	input wire[1:0] iS,
	input wire [SIZE-1:0] iI0, iI1,iI2, iI3,
	output reg [SIZE-1:0] oF
);

always @ (*)
begin
	case (iS)
		2'b00: oF = iI0;
		2'b01: oF = iI1;
		2'b10: oF = iI2;
		2'b11: oF = iI3;
		default:
			oF = 0;
	endcase
end

endmodule

//----------------------------------------------------
module MUX4_1
(
	input wire [3:0] iI0,
	input wire [3:0] iI1,
	input wire [3:0] iI2,
	input wire [3:0] iI3, 
	input wire [1:0] iS,
	output reg [3:0] oF
);

always @(*) 
	begin
	if     (iS==2'b00) oF=iI0;
	else if(iS==2'b01) oF=iI1;
	else if(iS==2'b10) oF=iI2;
	else if(iS==2'b11) oF=iI3;
	else oF=0;
	end
endmodule

//----------------------------------------------------


module MULMUX
(
	input wire [15:0] iA, iB,
	output wire [31:0] oR	
);

wire [3:0] wRAUX1;
wire [5:0] wRAUX2;
wire [7:0] wRAUX3;
wire [9:0] wRAUX4;
wire [11:0] wRAUX5;
wire [13:0] wRAUX6;
wire [15:0] wRAUX7;
wire [17:0] wRAUX8;
assign wRAUX2[1:0] = 2'b0;
assign wRAUX3[3:0] = 4'b0;
assign wRAUX4[5:0] = 6'b0;
assign wRAUX5[7:0] = 8'b0;
assign wRAUX6[9:0] = 10'b0;
assign wRAUX7[11:0] = 12'b0;
assign wRAUX8[13:0] = 14'b0;
assign oR = wRAUX1+wRAUX2+wRAUX3+wRAUX4+wRAUX5+wRAUX6+wRAUX7+wRAUX8;

MUX_FULL_PARALLEL MUX_1
(
	.iI0(0),
	.iI1(iA),
	.iI2(iA << 1),
	.iI3((iA << 1) + iA),
	.iS(iB[1:0]),
	.oF(wRAUX1)
);
 
 
MUX_FULL_PARALLEL MUX_2
(
	.iI0(0),
	.iI1(iA),
	.iI2(iA << 1),
	.iI3((iA << 1) + iA),
	.iS(iB[3:2]),
	.oF(wRAUX2[5:2])
);

MUX_FULL_PARALLEL MUX_3
(
	.iI0(0),
	.iI1(iA),
	.iI2(iA << 1),
	.iI3((iA << 1) + iA),
	.iS(iB[5:4]),
	.oF(wRAUX3[7:4])
);
 
 
MUX_FULL_PARALLEL MUX_4
(
	.iI0(0),
	.iI1(iA),
	.iI2(iA << 1),
	.iI3((iA << 1) + iA),
	.iS(iB[7:6]),
	.oF(wRAUX4[9:6])
);

MUX_FULL_PARALLEL MUX_5
(
	.iI0(0),
	.iI1(iA),
	.iI2(iA << 1),
	.iI3((iA << 1) + iA),
	.iS(iB[9:8]),
	.oF(wRAUX5[11:8])
);
 
 
MUX_FULL_PARALLEL MUX_6
(
	.iI0(0),
	.iI1(iA),
	.iI2(iA << 1),
	.iI3((iA << 1) + iA),
	.iS(iB[11:10]),
	.oF(wRAUX6[13:10])
);

MUX_FULL_PARALLEL MUX_7
(
	.iI0(0),
	.iI1(iA),
	.iI2(iA << 1),
	.iI3((iA << 1) + iA),
	.iS(iB[13:12]),
	.oF(wRAUX7[15:12])
);
 
 
MUX_FULL_PARALLEL MUX_8
(
	.iI0(0),
	.iI1(iA),
	.iI2(iA << 1),
	.iI3((iA << 1) + iA),
	.iS(iB[15:14]),
	.oF(wRAUX8[17:14])
);


endmodule
