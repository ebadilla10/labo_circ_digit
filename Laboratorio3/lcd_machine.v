`timescale 1ns / 1ps

`define STATE_RESET 0
`define STATE_POWERON_INIT_WAIT_0_A 1
`define STATE_POWERON_INIT_WAIT_0_B 2
`define STATE_POWERON_INIT_WRITE_1_A 3
`define STATE_POWERON_INIT_WRITE_1_B 4
`define STATE_POWERON_INIT_WAIT_2_A 5
`define STATE_POWERON_INIT_WAIT_2_B 6
`define STATE_POWERON_INIT_WRITE_3_A 7
`define STATE_POWERON_INIT_WRITE_3_B 8
`define STATE_POWERON_INIT_WAIT_4_A 9
`define STATE_POWERON_INIT_WAIT_4_B 10
`define STATE_POWERON_INIT_WRITE_5_A 11
`define STATE_POWERON_INIT_WRITE_5_B 12
`define STATE_POWERON_INIT_WAIT_6_A 13
`define STATE_POWERON_INIT_WAIT_6_B 14
`define STATE_POWERON_INIT_WRITE_7_A 15
`define STATE_POWERON_INIT_WRITE_7_B 16
`define STATE_POWERON_INIT_WAIT_8_A 17
`define STATE_POWERON_INIT_WAIT_8_B 18
`define STATE_OPERATION_UPPER_0_A 19
`define STATE_OPERATION_UPPER_0_B 20
`define STATE_OPERATION_WAIT_NIBBLE_0_1 21
`define STATE_OPERATION_WAIT_NIBBLE_0_2 22 
`define STATE_OPERATION_LOWER_0_A 23
`define STATE_OPERATION_LOWER_0_B 24
`define STATE_OPERATION_WAIT_BYTE_0_1 25
`define STATE_OPERATION_WAIT_BYTE_0_2 26
`define STATE_OPERATION_UPPER_1_A 27
`define STATE_OPERATION_UPPER_1_B 28
`define STATE_OPERATION_WAIT_NIBBLE_1_1 29
`define STATE_OPERATION_WAIT_NIBBLE_1_2 30
`define STATE_OPERATION_LOWER_1_A 31
`define STATE_OPERATION_LOWER_1_B 32
`define STATE_OPERATION_WAIT_BYTE_1_1 33
`define STATE_OPERATION_WAIT_BYTE_1_2 34
`define STATE_OPERATION_UPPER_2_A 35
`define STATE_OPERATION_UPPER_2_B 36
`define STATE_OPERATION_WAIT_NIBBLE_2_1 37
`define STATE_OPERATION_WAIT_NIBBLE_2_2 38
`define STATE_OPERATION_LOWER_2_A 39
`define STATE_OPERATION_LOWER_2_B 40
`define STATE_OPERATION_WAIT_BYTE_3_1 41
`define STATE_OPERATION_WAIT_BYTE_3_2 42
`define STATE_OPERATION_UPPER_3_A 43
`define STATE_OPERATION_UPPER_3_B 44
`define STATE_OPERATION_WAIT_NIBBLE_3_1 45
`define STATE_OPERATION_WAIT_NIBBLE_3_2 46
`define STATE_OPERATION_LOWER_3_A 47
`define STATE_OPERATION_LOWER_3_B 48
`define STATE_OPERATION_WAIT_BYTE_4_1 49
`define STATE_OPERATION_WAIT_BYTE_4_2 50
`define STATE_OPERATION_WAIT_4_A 51
`define STATE_OPERATION_WAIT_4_B 52
`define STATE_DISPLAY_UPPER_1_A 53
`define STATE_DISPLAY_UPPER_1_B 54
`define STATE_DISPLAY_WAIT_NIBBLE_0_1 55
`define STATE_DISPLAY_WAIT_NIBBLE_0_2 56
`define STATE_DISPLAY_LOWER_1_A 57
`define STATE_DISPLAY_LOWER_1_B 58
`define STATE_DISPLAY_WAIT_BYTE_1_1 59
`define STATE_DISPLAY_WAIT_BYTE_1_2 60
`define STATE_FINISH 61

module Module_LCD_Control
(
input wire Clock,
input wire Reset,
output wire oLCD_Enabled,
output reg oLCD_RegisterSelect, //0=Command, 1=Data
output wire oLCD_StrataFlashControl,
output wire oLCD_ReadWrite,
output reg[3:0] oLCD_Data
);
 
reg rWrite_Enabled;

assign oLCD_ReadWrite = 0; //I only Write to the LCD display, never Read from it
assign oLCD_StrataFlashControl = 1; //StrataFlash disabled. Full read/write access to LCD
//assign oLCD_Enabled = 1; 
assign oLCD_Enabled = rWrite_Enabled;

reg [7:0] rCurrentState,rNextState;
reg [31:0] rTimeCount;
reg rTimeCountReset;
wire wWriteDone;

//----------------------------------------------
//Next State and delay logic


always @ ( posedge Clock )
begin
  if (Reset)
  begin
        rCurrentState <= `STATE_RESET;
        rTimeCount    <= 32'b0;
  end
  else
  begin
    if (rTimeCountReset)
         rTimeCount <= 32'b0;
    else
         rTimeCount <= rTimeCount + 32'b1;
			
		rCurrentState <= rNextState;
	 
  end
end

//----------------------------------------------
//Current state and output logic
always @ ( * )
begin
        case (rCurrentState)
        //------------------------------------------
        `STATE_RESET:
        begin
                  rWrite_Enabled      = 1'b0;
                  oLCD_Data           = 4'h0;
                  oLCD_RegisterSelect = 1'b0;
                  rTimeCountReset     = 1'b0;

                  rNextState = `STATE_POWERON_INIT_WAIT_0_A;
        end
        //------------------------------------------
        /*
        Wait 15 ms or longer.
        The 15 ms interval is 750,000 clock cycles at 50 MHz.
        */

        `STATE_POWERON_INIT_WAIT_0_A:
				  begin
                  rWrite_Enabled       = 1'b0;
                  oLCD_Data            = 4'h0;
                  oLCD_RegisterSelect  = 1'b0; //these are commands
                  rTimeCountReset      = 1'b0;

                  if (rTimeCount > 32'd750000 )
                      rNextState    = `STATE_POWERON_INIT_WAIT_0_B;
                  else
                      rNextState    = `STATE_POWERON_INIT_WAIT_0_A;
                  end
        //------------------------------------------
        `STATE_POWERON_INIT_WAIT_0_B: 				
                  begin
                  rWrite_Enabled       = 1'b0;
                  oLCD_Data            = 4'h0;
                  oLCD_RegisterSelect  = 1'b0; //these are commands
                  rTimeCountReset      = 1'b1; //Reset the counter here

                  rNextState           = `STATE_POWERON_INIT_WRITE_1_A;
				  end
        //------------------------------------------
        /*
        Write SF_D<11:8> = 0x3, pulse LCD_E High for 12 clock cycles + 2 clock cycles (setup time)
        */
        `STATE_POWERON_INIT_WRITE_1_A:
        begin
                rWrite_Enabled       = 1'b1;
                oLCD_Data            = 4'h3;
                oLCD_RegisterSelect  = 1'b0; //these are commands
                rTimeCountReset      = 1'b0;

				if (rTimeCount > 32'd14 )
                      rNextState    = `STATE_POWERON_INIT_WRITE_1_B;
                else
                      rNextState    = `STATE_POWERON_INIT_WRITE_1_A;
		end
        //------------------------------------------
		`STATE_POWERON_INIT_WRITE_1_B:
        begin
                rWrite_Enabled       = 1'b0;
                oLCD_Data            = 4'h3;
                oLCD_RegisterSelect  = 1'b0; //these are commands
                rTimeCountReset      = 1'b1; 
				
                      rNextState    = `STATE_POWERON_INIT_WAIT_2_A;
		end
        //------------------------------------------
        /*
        Wait 4.1 ms or longer, which is 205,000 clock cycles at 50 MHz.
        */
        `STATE_POWERON_INIT_WAIT_2_A:
        begin
                rWrite_Enabled      = 1'b0;
                oLCD_Data           = 4'h3;
                oLCD_RegisterSelect = 1'b0; //these are commands
                rTimeCountReset     = 1'b0;

                if (rTimeCount > 32'd205000 )
                        rNextState      = `STATE_POWERON_INIT_WAIT_2_B;
                else
                        rNextState      = `STATE_POWERON_INIT_WAIT_2_A;
        end
        //------------------------------------------
        `STATE_POWERON_INIT_WAIT_2_B:
        begin
                rWrite_Enabled      = 1'b0;
                oLCD_Data           = 4'h3;
                oLCD_RegisterSelect = 1'b0; //these are commands
                rTimeCountReset     = 1'b1; //reset the time counter

                rNextState          = `STATE_POWERON_INIT_WRITE_3_A;
        end
        //------------------------------------------
		/*
        rite SF_D<11:8> = 0x3, pulse LCD_E High for 12 clock cycles + 2 clock cycles (setup time)
        */
        `STATE_POWERON_INIT_WRITE_3_A:
        begin
                rWrite_Enabled      = 1'b1;
                oLCD_Data           = 4'h3;
                oLCD_RegisterSelect = 1'b0; //these are commands
                rTimeCountReset     = 1'b0;
                if (rTimeCount > 32'd14 )
                        rNextState      = `STATE_POWERON_INIT_WRITE_3_B;
                else
                        rNextState      = `STATE_POWERON_INIT_WRITE_3_A;
        end
		//------------------------------------------
		`STATE_POWERON_INIT_WRITE_3_B:
        begin
                rWrite_Enabled       = 1'b0;
                oLCD_Data            = 4'h3;
                oLCD_RegisterSelect  = 1'b0; //these are commands
                rTimeCountReset      = 1'b1;
				
                      rNextState    = `STATE_POWERON_INIT_WAIT_4_A;
		end
        //------------------------------------------
        /*
        Wait 100 us or longer, which is 5,000 clock cycles at 50 MHz.
        */
        `STATE_POWERON_INIT_WAIT_4_A:
        begin
                rWrite_Enabled      = 1'b0;
                oLCD_Data           = 4'h3;
                oLCD_RegisterSelect = 1'b0; //these are commands
                rTimeCountReset     = 1'b0;

                if (rTimeCount > 32'd5000 )
                        rNextState      = `STATE_POWERON_INIT_WAIT_4_B;
                else
                        rNextState      = `STATE_POWERON_INIT_WAIT_4_A;
        end
        //------------------------------------------
        `STATE_POWERON_INIT_WAIT_4_B:
        begin
                rWrite_Enabled      = 1'b0;
                oLCD_Data           = 4'h3;
                oLCD_RegisterSelect = 1'b0; //these are commands
                rTimeCountReset     = 1'b1; //reset the time counter

                rNextState          = `STATE_POWERON_INIT_WRITE_5_A;
        end
		 //------------------------------------------
        /*
        Write SF_D<11:8> = 0x3, pulse LCD_E High for 12 clock cycles + 2 clock cycles (setup time)
        */
        `STATE_POWERON_INIT_WRITE_5_A:
        begin
                rWrite_Enabled       = 1'b1;
                oLCD_Data            = 4'h3;
                oLCD_RegisterSelect  = 1'b0; //these are commands
                rTimeCountReset      = 1'b0;

				if (rTimeCount > 32'd14 )
                      rNextState    = `STATE_POWERON_INIT_WRITE_5_B;
                else
                      rNextState    = `STATE_POWERON_INIT_WRITE_5_A;
      end
		
		`STATE_POWERON_INIT_WRITE_5_B:
        begin
                rWrite_Enabled      = 1'b0;
                oLCD_Data           = 4'h2;
                oLCD_RegisterSelect = 1'b0; //these are commands
                rTimeCountReset     = 1'b1;

                        rNextState      = `STATE_POWERON_INIT_WAIT_6_A;
        end
		//------------------------------------------
		/*
        Wait 40 us or longer, which is 2,000 clock cycles at 50 MHz.
        */
        `STATE_POWERON_INIT_WAIT_6_A:
        begin
                rWrite_Enabled      = 1'b0;
                oLCD_Data           = 4'h3;
                oLCD_RegisterSelect = 1'b0; //these are commands
                rTimeCountReset     = 1'b0;

                if (rTimeCount > 32'd2000 )
                        rNextState      = `STATE_POWERON_INIT_WAIT_6_B;
                else
                        rNextState      = `STATE_POWERON_INIT_WAIT_6_A;
        end
        //------------------------------------------
        `STATE_POWERON_INIT_WAIT_6_B:
        begin
                rWrite_Enabled      = 1'b0;
                oLCD_Data           = 4'h3;
                oLCD_RegisterSelect = 1'b0; //these are commands
                rTimeCountReset     = 1'b1; //reset the time counter

                rNextState          = `STATE_POWERON_INIT_WRITE_7_A;
        end
		 //------------------------------------------
        /*
        Write SF_D<11:8> = 0x3, pulse LCD_E High for 12 clock cycles + 2 clock cycles (setup time)
        */
        `STATE_POWERON_INIT_WRITE_7_A:
        begin
                rWrite_Enabled       = 1'b1;
                oLCD_Data            = 4'h2;
                oLCD_RegisterSelect  = 1'b0; //these are commands
                rTimeCountReset      = 1'b0;

				if (rTimeCount > 32'd14 )
                      rNextState    = `STATE_POWERON_INIT_WRITE_7_B;
                else
                      rNextState    = `STATE_POWERON_INIT_WRITE_7_A;
      end
		
		`STATE_POWERON_INIT_WRITE_7_B:
        begin
                rWrite_Enabled      = 1'b0;
                oLCD_Data           = 4'h2;
                oLCD_RegisterSelect = 1'b0; //these are commands
                rTimeCountReset     = 1'b1;

                        rNextState      = `STATE_POWERON_INIT_WAIT_8_A;
        end
		//------------------------------------------
		/*
        Wait 40 us or longer, which is 2,000 clock cycles at 50 MHz.
        */
        `STATE_POWERON_INIT_WAIT_8_A:
        begin
                rWrite_Enabled      = 1'b0;
                oLCD_Data           = 4'h2;
                oLCD_RegisterSelect = 1'b0; //these are commands
                rTimeCountReset     = 1'b0;

                if (rTimeCount > 32'd2000 )
                        rNextState      = `STATE_POWERON_INIT_WAIT_8_B;
                else
                        rNextState      = `STATE_POWERON_INIT_WAIT_8_A;
        end
        //------------------------------------------
        `STATE_POWERON_INIT_WAIT_8_B:
        begin
                rWrite_Enabled      = 1'b0;
                oLCD_Data           = 4'h2;
                oLCD_RegisterSelect = 1'b0; //these are commands
                rTimeCountReset     = 1'b1; //reset the time counter

                rNextState          = `STATE_OPERATION_UPPER_0_A;
        end
		//------------------------------------------ CHECKED 
		/*
        Write DB<7:4> for 12 clock cycles + 2 clock cycles (setup time) 
        */
        `STATE_OPERATION_UPPER_0_A:
        begin
                rWrite_Enabled      = 1'b1;
                oLCD_Data           = 4'h2;
                oLCD_RegisterSelect = 1'b0; //these are commands
                rTimeCountReset     = 1'b0;

                if (rTimeCount > 32'd14 )
                        rNextState      = `STATE_OPERATION_UPPER_0_B;
                else
                        rNextState      = `STATE_OPERATION_UPPER_0_A;
        end
        //------------------------------------------
        `STATE_OPERATION_UPPER_0_B:
        begin
                rWrite_Enabled      = 1'b0;
                oLCD_Data           = 4'h2;
                oLCD_RegisterSelect = 1'b0; //these are commands
                rTimeCountReset     = 1'b1; //reset the time counter

                rNextState          = `STATE_OPERATION_WAIT_NIBBLE_0_1;
        end
		//------------------------------------------
		/*
        Wait 1us between nibbles
        */
        `STATE_OPERATION_WAIT_NIBBLE_0_1:
        begin
                rWrite_Enabled      = 1'b0;
                oLCD_Data           = 4'h2;
                oLCD_RegisterSelect = 1'b0; //these are commands
                rTimeCountReset     = 1'b0; //reset the time counter

                if (rTimeCount > 32'd50 )
                        rNextState      = `STATE_OPERATION_WAIT_NIBBLE_0_2;
                else
                        rNextState      = `STATE_OPERATION_WAIT_NIBBLE_0_1;
        end
		//------------------------------------------
        `STATE_OPERATION_WAIT_NIBBLE_0_2:
        begin
                rWrite_Enabled      = 1'b0;
                oLCD_Data           = 4'h0;
                oLCD_RegisterSelect = 1'b0; //these are commands
                rTimeCountReset     = 1'b1; //reset the time counter

                rNextState          = `STATE_OPERATION_LOWER_0_A;
        end
		//------------------------------------------
		//------------------------------------------
		/*
        Write DB<3:0> for 12 clock cycles + 2 clock cycles (setup time)
        */
        `STATE_OPERATION_LOWER_0_A:
        begin
                rWrite_Enabled      = 1'b1;
                oLCD_Data           = 4'h8;
                oLCD_RegisterSelect = 1'b0; //these are commands
                rTimeCountReset     = 1'b0;

                if (rTimeCount > 32'd14 )
                        rNextState      = `STATE_OPERATION_LOWER_0_B;
                else
                        rNextState      = `STATE_OPERATION_LOWER_0_A;
        end
        //------------------------------------------
        `STATE_OPERATION_LOWER_0_B:
        begin
                rWrite_Enabled      = 1'b0;
                oLCD_Data           = 4'h2;
                oLCD_RegisterSelect = 1'b0; //these are commands
                rTimeCountReset     = 1'b1; //reset the time counter

                rNextState          = `STATE_OPERATION_WAIT_BYTE_0_1;
        end
		//------------------------------------------
		/*
        Wait 40us between bytes
        */
        `STATE_OPERATION_WAIT_BYTE_0_1:
        begin
                rWrite_Enabled      = 1'b0;
                oLCD_Data           = 4'h8; // no se
                oLCD_RegisterSelect = 1'b0; //these are commands
                rTimeCountReset     = 1'b0;

                if (rTimeCount > 32'd2000 )
                        rNextState      = `STATE_OPERATION_WAIT_BYTE_0_2;
                else
                        rNextState      = `STATE_OPERATION_WAIT_BYTE_0_1;
        end
        //------------------------------------------
		`STATE_OPERATION_WAIT_BYTE_0_2:
        begin
                rWrite_Enabled      = 1'b0;
                oLCD_Data           = 4'h2;
                oLCD_RegisterSelect = 1'b0; //these are commands
                rTimeCountReset     = 1'b1; //reset the time counter

                rNextState          = `STATE_OPERATION_UPPER_1_A;
        end
		/*
        Write DB<7:4> for 12 clock cycles + 2 clock cycles (setup time) 
        */
        `STATE_OPERATION_UPPER_1_A:
        begin
                rWrite_Enabled      = 1'b1;
                oLCD_Data           = 4'h0;
                oLCD_RegisterSelect = 1'b0; //these are commands
                rTimeCountReset     = 1'b0;

                if (rTimeCount > 32'd14 )
                        rNextState      = `STATE_OPERATION_UPPER_1_B;
                else
                        rNextState      = `STATE_OPERATION_UPPER_1_A;
        end
        //------------------------------------------
        `STATE_OPERATION_UPPER_1_B:
        begin
                rWrite_Enabled      = 1'b0;
                oLCD_Data           = 4'h0;
                oLCD_RegisterSelect = 1'b0; //these are commands
                rTimeCountReset     = 1'b1; //reset the time counter

                rNextState          = `STATE_OPERATION_WAIT_NIBBLE_1_1;
        end
		//------------------------------------------
		/*
        Wait 1us between nibbles
        */
        `STATE_OPERATION_WAIT_NIBBLE_1_1:
        begin
                rWrite_Enabled      = 1'b0;
                oLCD_Data           = 4'h2;
                oLCD_RegisterSelect = 1'b0; //these are commands
                rTimeCountReset     = 1'b0; //reset the time counter

                if (rTimeCount > 32'd50 )
                        rNextState      = `STATE_OPERATION_WAIT_NIBBLE_1_2;
                else
                        rNextState      = `STATE_OPERATION_WAIT_NIBBLE_1_1;
        end
		//------------------------------------------
        `STATE_OPERATION_WAIT_NIBBLE_1_2:
        begin
                rWrite_Enabled      = 1'b0;
                oLCD_Data           = 4'h0;
                oLCD_RegisterSelect = 1'b0; //these are commands
                rTimeCountReset     = 1'b1; //reset the time counter

                rNextState          = `STATE_OPERATION_LOWER_1_A;
        end
		//------------------------------------------
		//------------------------------------------
		/*
        Write DB<3:0> for 12 clock cycles + 2 clock cycles (setup time)
        */
        `STATE_OPERATION_LOWER_1_A:
        begin
                rWrite_Enabled      = 1'b1;
                oLCD_Data           = 4'h6;
                oLCD_RegisterSelect = 1'b0; //these are commands
                rTimeCountReset     = 1'b0;

                if (rTimeCount > 32'd14 )
                        rNextState      = `STATE_OPERATION_LOWER_1_B;
                else
                        rNextState      = `STATE_OPERATION_LOWER_1_A;
        end
        //------------------------------------------
        `STATE_OPERATION_LOWER_1_B:
        begin
                rWrite_Enabled      = 1'b0;
                oLCD_Data           = 4'h6;
                oLCD_RegisterSelect = 1'b0; //these are commands
                rTimeCountReset     = 1'b1; //reset the time counter

                rNextState          = `STATE_OPERATION_WAIT_BYTE_1_1;
        end
		//------------------------------------------
		/*
        Wait 40us between bytes
        */
        `STATE_OPERATION_WAIT_BYTE_1_1:
        begin
                rWrite_Enabled      = 1'b0;
                oLCD_Data           = 4'h8; // no se
                oLCD_RegisterSelect = 1'b0; //these are commands
                rTimeCountReset     = 1'b0;

                if (rTimeCount > 32'd2000 )
                        rNextState      = `STATE_OPERATION_WAIT_BYTE_1_2;
                else
                        rNextState      = `STATE_OPERATION_WAIT_BYTE_1_1;
        end
		//------------------------------------------
		`STATE_OPERATION_WAIT_BYTE_1_2:
        begin
                rWrite_Enabled      = 1'b0;
                oLCD_Data           = 4'h2;
                oLCD_RegisterSelect = 1'b0; //these are commands
                rTimeCountReset     = 1'b1; //reset the time counter

                rNextState          = `STATE_OPERATION_UPPER_2_A;
        end
		/*
        Write DB<7:4> for 12 clock cycles + 2 clock cycles (setup time) 
        */
        `STATE_OPERATION_UPPER_2_A:
        begin
                rWrite_Enabled      = 1'b1;
                oLCD_Data           = 4'h0;
                oLCD_RegisterSelect = 1'b0; //these are commands
                rTimeCountReset     = 1'b0;

                if (rTimeCount > 32'd14 )
                        rNextState      = `STATE_OPERATION_UPPER_2_B;
                else
                        rNextState      = `STATE_OPERATION_UPPER_2_A;
        end
        //------------------------------------------
        `STATE_OPERATION_UPPER_2_B:
        begin
                rWrite_Enabled      = 1'b0;
                oLCD_Data           = 4'h0;
                oLCD_RegisterSelect = 1'b0; //these are commands
                rTimeCountReset     = 1'b1; //reset the time counter

                rNextState          = `STATE_OPERATION_WAIT_NIBBLE_2_1;
        end
		//------------------------------------------
		/*
        Wait 1us between nibbles
        */
        `STATE_OPERATION_WAIT_NIBBLE_2_1:
        begin
                rWrite_Enabled      = 1'b0;
                oLCD_Data           = 4'h0;
                oLCD_RegisterSelect = 1'b0; //these are commands
                rTimeCountReset     = 1'b0; //reset the time counter

                if (rTimeCount > 32'd50 )
                        rNextState      = `STATE_OPERATION_WAIT_NIBBLE_2_2;
                else
                        rNextState      = `STATE_OPERATION_WAIT_NIBBLE_2_1;
        end
		//------------------------------------------
        `STATE_OPERATION_WAIT_NIBBLE_2_2:
        begin
                rWrite_Enabled      = 1'b0;
                oLCD_Data           = 4'h0;
                oLCD_RegisterSelect = 1'b0; //these are commands
                rTimeCountReset     = 1'b1; //reset the time counter

                rNextState          = `STATE_OPERATION_LOWER_2_A;
        end
		//------------------------------------------
		//------------------------------------------
		/*
        Write DB<3:0> for 12 clock cycles + 2 clock cycles (setup time)
        */
        `STATE_OPERATION_LOWER_2_A:
        begin
                rWrite_Enabled      = 1'b1;
                oLCD_Data           = 4'hC;
                oLCD_RegisterSelect = 1'b0; //these are commands
                rTimeCountReset     = 1'b0;

                if (rTimeCount > 32'd14 )
                        rNextState      = `STATE_OPERATION_LOWER_2_B;
						
                else
                        rNextState      = `STATE_OPERATION_LOWER_2_A;
        end
        //------------------------------------------
        `STATE_OPERATION_LOWER_2_B:
        begin
                rWrite_Enabled      = 1'b0;
                oLCD_Data           = 4'h2;
                oLCD_RegisterSelect = 1'b0; //these are commands
                rTimeCountReset     = 1'b1; //reset the time counter

                rNextState          = `STATE_OPERATION_WAIT_BYTE_3_1;
        end
		//------------------------------------------
		/*
        Wait 40us between bytes
        */
		`STATE_OPERATION_WAIT_BYTE_3_1:
        begin
                rWrite_Enabled      = 1'b0;
                oLCD_Data           = 4'h8; // no se
                oLCD_RegisterSelect = 1'b0; //these are commands
                rTimeCountReset     = 1'b0;

                if (rTimeCount > 32'd2000 )
                        rNextState      = `STATE_OPERATION_WAIT_BYTE_3_2;
                else
                        rNextState      = `STATE_OPERATION_WAIT_BYTE_3_1;
        end
		//------------------------------------------
		`STATE_OPERATION_WAIT_BYTE_3_2:
        begin
                rWrite_Enabled      = 1'b0;
                oLCD_Data           = 4'h2;
                oLCD_RegisterSelect = 1'b0; //these are commands
                rTimeCountReset     = 1'b1; //reset the time counter

                rNextState          = `STATE_OPERATION_UPPER_3_A;
        end
		
		`STATE_OPERATION_UPPER_3_A:
        begin
                rWrite_Enabled      = 1'b1;
                oLCD_Data           = 4'h0;
                oLCD_RegisterSelect = 1'b0; //these are commands
                rTimeCountReset     = 1'b0;

                if (rTimeCount > 32'd14 )
                        rNextState      = `STATE_OPERATION_UPPER_3_B;
                else
                        rNextState      = `STATE_OPERATION_UPPER_3_A;
        end
        //------------------------------------------
        `STATE_OPERATION_UPPER_3_B:
        begin
                rWrite_Enabled      = 1'b0;
                oLCD_Data           = 4'h0;
                oLCD_RegisterSelect = 1'b0; //these are commands
                rTimeCountReset     = 1'b1; //reset the time counter

                rNextState          = `STATE_OPERATION_WAIT_NIBBLE_3_1;
        end
		//------------------------------------------
		/*
        Wait 1us between nibbles
        */
        `STATE_OPERATION_WAIT_NIBBLE_3_1:
        begin
                rWrite_Enabled      = 1'b0;
                oLCD_Data           = 4'h0;
                oLCD_RegisterSelect = 1'b0; //these are commands
                rTimeCountReset     = 1'b0; //reset the time counter

                if (rTimeCount > 32'd50 )
                        rNextState      = `STATE_OPERATION_WAIT_NIBBLE_3_2;
                else
                        rNextState      = `STATE_OPERATION_WAIT_NIBBLE_3_1;
        end
		//------------------------------------------
        `STATE_OPERATION_WAIT_NIBBLE_3_2:
        begin
                rWrite_Enabled      = 1'b0;
                oLCD_Data           = 4'h0;
                oLCD_RegisterSelect = 1'b0; //these are commands
                rTimeCountReset     = 1'b1; //reset the time counter

                rNextState          = `STATE_OPERATION_LOWER_3_A;
        end
		//------------------------------------------
		/*
        Write DB<3:0> for 12 clock cycles + 2 clock cycles (setup time)
        */
        `STATE_OPERATION_LOWER_3_A:
        begin
                rWrite_Enabled      = 1'b1;
                oLCD_Data           = 4'h1;
                oLCD_RegisterSelect = 1'b0; //these are commands
                rTimeCountReset     = 1'b0;

                if (rTimeCount > 32'd14 )
                        rNextState      = `STATE_OPERATION_LOWER_3_B;
						
                else
                        rNextState      = `STATE_OPERATION_LOWER_3_A;
        end
        //------------------------------------------
        `STATE_OPERATION_LOWER_3_B:
        begin
                rWrite_Enabled      = 1'b0;
                oLCD_Data           = 4'h1;
                oLCD_RegisterSelect = 1'b0; //these are commands
                rTimeCountReset     = 1'b1; //reset the time counter

                rNextState          = `STATE_OPERATION_WAIT_BYTE_4_1;
        end
		//------------------------------------------
		/*
        Wait 40us between bytes
        */
		`STATE_OPERATION_WAIT_BYTE_4_1:
        begin
                rWrite_Enabled      = 1'b0;
                oLCD_Data           = 4'h8; // no se
                oLCD_RegisterSelect = 1'b0; //these are commands
                rTimeCountReset     = 1'b0;

                if (rTimeCount > 32'd2000 )
                        rNextState      = `STATE_OPERATION_WAIT_BYTE_4_2;
                else
                        rNextState      = `STATE_OPERATION_WAIT_BYTE_4_1;
        end
		//------------------------------------------
		`STATE_OPERATION_WAIT_BYTE_4_2:
        begin
                rWrite_Enabled      = 1'b0;
                oLCD_Data           = 4'h2;
                oLCD_RegisterSelect = 1'b0; //these are commands
                rTimeCountReset     = 1'b1; //reset the time counter

                rNextState          = `STATE_OPERATION_WAIT_4_A;
        end
		
		
		/*
        Wait 1.64ms between bytes
        */
		`STATE_OPERATION_WAIT_4_A:
        begin
                rWrite_Enabled      = 1'b0;
                oLCD_Data           = 4'h1; // no se
                oLCD_RegisterSelect = 1'b0; //these are commands
                rTimeCountReset     = 1'b0;

                if (rTimeCount > 32'd82000 )
                        rNextState      = `STATE_OPERATION_WAIT_4_B;
                else
                        rNextState      = `STATE_OPERATION_WAIT_4_A;
        end
		`STATE_OPERATION_WAIT_4_B:
        begin
                rWrite_Enabled      = 1'b0;
                oLCD_Data           = 4'h1;
                oLCD_RegisterSelect = 1'b0; //these are commands
                rTimeCountReset     = 1'b1; //reset the time counter

                rNextState          = `STATE_DISPLAY_UPPER_1_A;
        end
		
		
		
		
		
		//Letra H 
		`STATE_DISPLAY_UPPER_1_A:
        begin
                rWrite_Enabled      = 1'b1;
                oLCD_Data           = 4'h4;
                oLCD_RegisterSelect = 1'b1; //these are commands
                rTimeCountReset     = 1'b0;

                if (rTimeCount > 32'd14 )
                        rNextState      = `STATE_DISPLAY_UPPER_1_B;
                else
                        rNextState      = `STATE_DISPLAY_UPPER_1_A;
        end
        //------------------------------------------
        `STATE_DISPLAY_UPPER_1_B:
        begin
                rWrite_Enabled      = 1'b0;
                oLCD_Data           = 4'h4;
                oLCD_RegisterSelect = 1'b1; //these are commands
                rTimeCountReset     = 1'b1; //reset the time counter

                rNextState          = `STATE_DISPLAY_WAIT_NIBBLE_0_1;
        end
		//------------------------------------------
		/*
        Wait 1us between nibbles
        */
        `STATE_DISPLAY_WAIT_NIBBLE_0_1:
        begin
                rWrite_Enabled      = 1'b0;
                oLCD_Data           = 4'h0;
                oLCD_RegisterSelect = 1'b1; //these are commands
                rTimeCountReset     = 1'b0; //reset the time counter

                if (rTimeCount > 32'd50 )
                        rNextState      = `STATE_DISPLAY_WAIT_NIBBLE_0_2;
                else
                        rNextState      = `STATE_DISPLAY_WAIT_NIBBLE_0_1;
        end
		//------------------------------------------
        `STATE_DISPLAY_WAIT_NIBBLE_0_2:
        begin
                rWrite_Enabled      = 1'b0;
                oLCD_Data           = 4'h4;
                oLCD_RegisterSelect = 1'b1; //these are commands
                rTimeCountReset     = 1'b1; //reset the time counter

                rNextState          = `STATE_DISPLAY_LOWER_1_A;
        end
		//------------------------------------------
		//------------------------------------------
		/*
        Write DB<3:0> for 12 clock cycles + 2 clock cycles (setup time)
        */
        `STATE_DISPLAY_LOWER_1_A:
        begin
                rWrite_Enabled      = 1'b1;
                oLCD_Data           = 4'h8;
                oLCD_RegisterSelect = 1'b1; //these are commands
                rTimeCountReset     = 1'b0;

                if (rTimeCount > 32'd14 )
                        rNextState      = `STATE_DISPLAY_LOWER_1_B;
						
                else
                        rNextState      = `STATE_DISPLAY_LOWER_1_A;
        end
        //------------------------------------------
        `STATE_DISPLAY_LOWER_1_B:
        begin
                rWrite_Enabled      = 1'b0;
                oLCD_Data           = 4'h8;
                oLCD_RegisterSelect = 1'b1; //these are commands
                rTimeCountReset     = 1'b1; //reset the time counter

                rNextState          = `STATE_DISPLAY_WAIT_BYTE_1_1;
        end
		//------------------------------------------
		/*
        Wait 40us between bytes
        */
		`STATE_DISPLAY_WAIT_BYTE_1_1:
        begin
                rWrite_Enabled      = 1'b0;
                oLCD_Data           = 4'h8; // no se
                oLCD_RegisterSelect = 1'b1; //these are commands
                rTimeCountReset     = 1'b0;

                if (rTimeCount > 32'd2000 )
                        rNextState      = `STATE_DISPLAY_WAIT_BYTE_1_2;
                else
                        rNextState      = `STATE_DISPLAY_WAIT_BYTE_1_1;
        end
		//------------------------------------------
		`STATE_DISPLAY_WAIT_BYTE_1_2:
		begin
                rWrite_Enabled      = 1'b0;
                oLCD_Data           = 4'h8;
                oLCD_RegisterSelect = 1'b1; //these are commands
                rTimeCountReset     = 1'b1; //reset the time counter

                rNextState          = `STATE_FINISH;
        end
		//------------------------------------------
		
		`STATE_FINISH:
		begin
                rWrite_Enabled      = 1'b0;
                oLCD_Data           = 4'h0;
                oLCD_RegisterSelect = 1'b0; //these are commands
                rTimeCountReset     = 1'b1; //reset the time counter
				
                        rNextState      = `STATE_FINISH;

        end
			
        default:
        begin
                rWrite_Enabled= 1'b0;
                oLCD_Data= 4'h0;
                oLCD_RegisterSelect= 1'b0;
                rTimeCountReset= 1'b0;

                rNextState = `STATE_RESET;
        end
        //------------------------------------------
        endcase

		
end
endmodule
