`timescale 1ns / 1ps
  
////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   22:30:52 01/30/2011
// Design Name:   MiniAlu
// Module Name:   D:/Proyecto/RTL/Dev/MiniALU/TestBench.v
// Project Name:  MiniALU
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: MiniAlu
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////
 
module TestBench;

	// Inputs
	reg Clock;
	reg Reset;
	
	// Outputs
	wire oLCD_Enabled, oLCD_RegisterSelect, oLCD_StrataFlashControl, oLCD_ReadWrite;
	wire [3:0] oLCD_Data;

	// Instantiate the Unit Under Test (UUT)
	Module_LCD_Control uut (
		.Clock(Clock), 
		.Reset(Reset),
		.oLCD_Enabled(oLCD_Enabled),		
		.oLCD_RegisterSelect(oLCD_RegisterSelect),
		.oLCD_StrataFlashControl(oLCD_StrataFlashControl),
		.oLCD_ReadWrite(oLCD_ReadWrite),
		.oLCD_Data(oLCD_Data)
	);
	
	always
	begin
		#1  Clock =  ! Clock;

	end

	initial begin
		// Initialize Inputs
		Clock = 0;
		Reset = 0;

		// Wait 100 ns for global reset to finish
		#100;
		Reset = 1;
		#50
		Reset = 0;
        
		// Add stimulus here
		// Primer estado = 0
		
		

	end
      
endmodule

