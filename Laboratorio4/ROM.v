`timescale 1ns / 1ps
`include "Defintions.v"

`define LOOP1 8'd6
`define LOOP2 8'd8
`define LOOP3 8'd15
`define LOOP4 8'd17
`define LOOP5 8'd24
`define LOOP6 8'd26
`define LOOP7 8'd33
`define LOOP8 8'd35


module ROM
(
	input  wire[15:0]  		iAddress,
	output reg [27:0] 		oInstruction
);	
always @ ( iAddress )
begin
	case (iAddress)

	0: oInstruction = { `NOP ,24'd4000	  	 };
	1: oInstruction = { `STO ,`R0,16'h0     };
	2: oInstruction = { `STO ,`R1,16'h0     };
	3: oInstruction = { `STO , `R6,16'd640 };
	4: oInstruction = { `STO , `R7,16'd120 };
	//Inicializar la memoria de video.
	
	5: oInstruction = { `STO , `R4,16'd0 };
	//LOOP1
	6: oInstruction = { `ADDI ,`R4,`R4,8'h1    }; 
	
	7: oInstruction = { `STO , `R5,16'd0 };
	//LOOP2
	8: oInstruction = { `ADDI ,`R5,`R5,8'h1    }; 
	
	
	9: oInstruction = { `ADDI ,`R0,`R0,8'h1    }; 
	10: oInstruction = { `ADDC ,`R1,`R1,8'b0    }; 
	11: oInstruction = { `VGA ,COLOR_GREEN,	5'b0 `R1,`R0};
	
	
	12: oInstruction = { `BLE ,`LOOP2,`R5,`R7 };
	13: oInstruction = { `BLE ,`LOOP1,`R4,`R6 };
	
	//-------New Color----------
	14: oInstruction = { `STO , `R4,16'd0 };
	//LOOP3
	15: oInstruction = { `ADDI ,`R4,`R4,8'h1    }; 
	
	16: oInstruction = { `STO , `R5,16'd0 };
	//LOOP4
	17: oInstruction = { `ADDI ,`R5,`R5,8'h1    }; 
	
	
	18: oInstruction = { `ADDI ,`R0,`R0,8'h1    }; 
	19: oInstruction = { `ADDC ,`R1,`R1,8'b0    }; 
	20: oInstruction = { `VGA ,COLOR_RED,	5'b0 `R1,`R0};
	
	
	21: oInstruction = { `BLE ,`LOOP4,`R5,`R7 };
	22: oInstruction = { `BLE ,`LOOP3,`R4,`R6 };
	
	//-------New Color----------
	23: oInstruction = { `STO , `R4,16'd0 };
	//LOOP5
	24: oInstruction = { `ADDI ,`R4,`R4,8'h1    }; 
	
	25: oInstruction = { `STO , `R5,16'd0 };
	//LOOP6
	26: oInstruction = { `ADDI ,`R5,`R5,8'h1    }; 
	
	
	27: oInstruction = { `ADDI ,`R0,`R0,8'h1    }; 
	28: oInstruction = { `ADDC ,`R1,`R1,8'b0    }; 
	29: oInstruction = { `VGA ,COLOR_MAGENTA,	5'b0 `R1,`R0};
	
	
	30: oInstruction = { `BLE ,`LOOP6,`R5,`R7 };
	31: oInstruction = { `BLE ,`LOOP5,`R4,`R6 };
	
	//-------New Color----------
	32: oInstruction = { `STO , `R4,16'd0 };
	//LOOP3
	33: oInstruction = { `ADDI ,`R4,`R4,8'h1    }; 
	
	34: oInstruction = { `STO , `R5,16'd0 };
	//LOOP4
	35: oInstruction = { `ADDI ,`R5,`R5,8'h1    }; 
	
	
	36: oInstruction = { `ADDI ,`R0,`R0,8'h1    }; 
	37: oInstruction = { `ADDC ,`R1,`R1,8'b0    }; 
	38: oInstruction = { `VGA ,COLOR_BLUE,	5'b0 `R1,`R0};
	
	
	21: oInstruction = { `BLE ,`LOOP4,`R5,`R7 };
	22: oInstruction = { `BLE ,`LOOP3,`R4,`R6 };
	
	

	 
	default:
		oInstruction = { `LED ,8'b0,`R3,8'b0 };
	endcase	
end
	
endmodule
